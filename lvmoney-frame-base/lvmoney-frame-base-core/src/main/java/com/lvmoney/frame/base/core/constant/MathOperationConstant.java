package com.lvmoney.frame.base.core.constant;/**
 * 描述:
 * 包名:com.lvmoney.frame.expression.constant
 * 版本信息: 版本1.0
 * 日期:2022/6/14
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/6/14 9:49
 */
public class MathOperationConstant {

    /**
     * &
     */
    public static final String OPERATION_SYMBOL_AND = "&";

    /**
     * &
     */
    public static final String OPERATION_SYMBOL_OR = "|";
    /**
     * &&
     */
    public static final String OPERATION_SYMBOL_AND_AND = "&&";

    /**
     * &&
     */
    public static final String OPERATION_SYMBOL_OR_OR = "||";

    /**
     * <
     */
    public static final String OPERATION_SYMBOL_LESS = "<";

    /**
     * <=
     */
    public static final String OPERATION_SYMBOL_LESS_EQUAL = "<=";


    /**
     * =
     */
    public static final String OPERATION_SYMBOL_EQUAL = "=";


    /**
     * ==
     */
    public static final String OPERATION_SYMBOL_EQUAL_EQUAL = "==";


    /**
     * >
     */
    public static final String OPERATION_SYMBOL_GREATER = ">";


    /**
     * >=
     */
    public static final String OPERATION_SYMBOL_GREATER_EQUAL = ">=";


    /**
     * !=
     */
    public static final String OPERATION_SYMBOL_EQUAL_NOT = "!=";

    /**
     * +
     */
    public static final String OPERATION_SYMBOL_PLUS = "+";


    /**
     * -
     */
    public static final String OPERATION_SYMBOL_REDUCE = "-";

    /**
     * *
     */
    public static final String OPERATION_SYMBOL_MULTIPLICATION = "*";

    /**
     * /
     */
    public static final String OPERATION_SYMBOL_DIVISION = "/";


    /**
     * **
     */
    public static final String OPERATION_SYMBOL_ASTERISK_ASTERISK = "**";


    /**
     * ^
     */
    public static final String OPERATION_SYMBOL_XOR = "^";


    /**
     * !
     */
    public static final String OPERATION_SYMBOL_NOT = "!";

    /**
     * %
     */
    public static final String OPERATION_SYMBOL_RESIDUAL = "%";


}
