import jieba

from CloudUtil import create_word_cloud
from Const import ENCODING_UTF8
from FilterUtil import seg_depart, count_from_file
from HtmlHandUtil import create_doc_from_filename, parse

from RequestUtil import download_content, save_to_file

if __name__ == '__main__':
    # 需要分析文档的路径
    filename = "./website.txt"
    outfilename = "./out.txt"
    # 过滤词的路径
    stopWord = './stop.txt'
    # 高频词结果
    filterResult = './filter.txt'
    # 字体路径
    fontPath = 'C:\Windows\Fonts\simkai.ttf'
    # 生成词云图片
    cloudPic = 'cloud_pic.png'
    # 筛选高频词的个数
    topLimit = 100

    websiteHtml = "website.html"
    nodeName = 'table'
    idName = 'myTable'

    # 需要爬虫的网站
    url = "http://gk.chengdu.gov.cn/govInfoPub/detail.action?id=133005&tn=2"
    result = download_content(url)
    save_to_file(websiteHtml, result)

    doc = create_doc_from_filename(websiteHtml)
    parse(doc, nodeName, idName, '', filename)
    inputs = open(filename, 'r', encoding='UTF-8')
    outputs = open(outfilename, 'w', encoding='UTF-8')
    jieba.add_word("停用词")
    line_seg = ''
    # 将输出结果写到out.txt中
    for line in inputs:
        line_seg += seg_depart(line, stopWord)

    outputs.write(line_seg + '\n')
    print("-------------------------正在分词和去停用词--------------------------")
    print("原文：" + line)
    print("去停用词：" + line_seg)

    outputs.close()
    inputs.close()
    print("删除停用词和分词成功！！！")

    print("打印词频==============")
    retult = count_from_file(outfilename, stopWord, top_limit=topLimit)

    print(type(retult))
    print(retult)
    cResult = open(filterResult, mode='w', encoding=ENCODING_UTF8)
    for num in retult:
        cResult.writelines(num[0] + ' ')  # \n 换行符
    cResult.close()
    print("*" * 100)

    create_word_cloud(filterResult, fontPath, cloudPic)
