package com.lvmoney.frame.ai.constant;/**
 * 描述:
 * 包名:com.lvmoney.frame.ai.constant
 * 版本信息: 版本1.0
 * 日期:2022/8/5
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/8/5 17:49
 */
public class AiConstant {
    /**
     * 数据智能统一前缀
     */
    public static final String SMART_DATA_PREFIX = "smartData";
}
